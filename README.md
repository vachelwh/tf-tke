# 创建TKE集群

## 集群规格
|  组件类型   | CPU   |  内存  | 磁盘         |
|  ----      | ----  | ----  | ----        | 
| Master     | 8c    |  16G  |  200G (SSD) |
| Workload   | 8c    |  16G  |  200G (SSD) |

### 安装Terrform命令行工具
```
# curl -sLk -o terrafrom https://github.com/hashicorp/terraform/releases
# mv terraform /usr/local/bin
# terraform --version

```

### 执行部署
#### 初始化
``` 
# terraform init
```

#### 执行部署前计划
```
# terraform plan
```

####  执行创建集群
```
# terraform apply
```
